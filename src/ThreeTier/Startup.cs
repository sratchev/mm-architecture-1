﻿using Microsoft.Owin;
using Owin;
using ThreeTier.Web;

[assembly: OwinStartup(typeof(Startup))]

namespace ThreeTier.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
