﻿using System.Net.Http;
using System.Net.Http.Formatting;
using System.Web.Http;
using System.Web.Http.Filters;
using Newtonsoft.Json.Serialization;
using ThreeTier.Web.ViewModels;

namespace ThreeTier.Web.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        [CamelCased]
        public IHttpActionResult Get()
        {
            return Ok(new ValueViewModel[]
            {
                new ValueViewModel()
                {
                    Id = 1,
                    Name = "One"
                },
                new ValueViewModel()
                {
                    Id = 2,
                    Name = "Two"
                },
            });
        }

        // GET api/values/5
        public IHttpActionResult Get(int id)
        {
            return Ok(new ValueViewModel[]
{
                new ValueViewModel()
                {
                    Id = 1,
                    Name = "One"
                },
                new ValueViewModel()
                {
                    Id = 2,
                    Name = "Two"
                },
});
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }


    //todo: slide 1
    public class CamelCasedAttribute : ActionFilterAttribute
    {
        private static readonly JsonMediaTypeFormatter CamelCasingFormatter;

        static CamelCasedAttribute()
        {
            CamelCasingFormatter = new JsonMediaTypeFormatter
            {
                SerializerSettings = { ContractResolver = new CamelCasePropertyNamesContractResolver() }
            };
        }

        public override void OnActionExecuted(HttpActionExecutedContext httpActionExecutedContext)
        {
            var objectContent = httpActionExecutedContext.Response.Content as ObjectContent;

            if (objectContent != null)
            {
                if (objectContent.Formatter is JsonMediaTypeFormatter)
                {
                    httpActionExecutedContext.Response.Content = new ObjectContent(objectContent.ObjectType, objectContent.Value, CamelCasingFormatter);
                }
            }
        }
    }
}
