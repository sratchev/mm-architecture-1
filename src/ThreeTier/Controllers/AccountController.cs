﻿using System;
using System.Web.Http;
using ThreeTier.Application.DomainEvents;
using ThreeTier.Application.Services;
using ThreeTier.Web.ViewModels;

namespace ThreeTier.Web.Controllers
{
    public class AccountController : ApiController
    {
        private readonly IDomainEvents _domainEvents;
        private readonly IUserService _userService;

        public AccountController(IDomainEvents domainEvents, IUserService userService)
        {
            if (domainEvents == null) throw new ArgumentNullException("domainEvents");
            if (userService == null) throw new ArgumentNullException("userRepository");

            _domainEvents = domainEvents;
            _userService = userService;
        }


        public IHttpActionResult EditProfile(AccountEditProfileViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (_userService.ChangeName(model.Id, model.FirstName, model.LastName))
            {
                return Ok();
            }

            return BadRequest("Name change failed");
        }
    }
}
