namespace ThreeTier.Web.ViewModels
{
    public class AccountEditProfileViewModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Id { get; set; }
    }
}