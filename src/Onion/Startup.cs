﻿using Microsoft.Owin;
using Onion.Web;
using Owin;

[assembly: OwinStartup(typeof(Startup))]

namespace Onion.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
