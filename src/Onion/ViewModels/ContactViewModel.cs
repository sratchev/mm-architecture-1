﻿namespace Onion.Web.ViewModels
{

    public class ValueViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
