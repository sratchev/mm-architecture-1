﻿using System;
using System.Web.Http;
using Onion.Core.Events;
using Onion.Core.Repository;
using Onion.Web.ViewModels;

namespace Onion.Web.Controllers
{
    public class AccountController : ApiController
    {
        private readonly IDomainEvents _domainEvents;
        private readonly IUserRepository _userRepository;

        public AccountController(IDomainEvents domainEvents, IUserRepository userRepository)
        {
            if (domainEvents == null) throw new ArgumentNullException("domainEvents");
            if (userRepository == null) throw new ArgumentNullException("userRepository");

            _domainEvents = domainEvents;
            _userRepository = userRepository;
        }


        public IHttpActionResult EditProfile(AccountEditProfileViewModel model)
        {
            var user = _userRepository.GetById(model.Id);

            //user.FirstName = model.FirstName;
            //user.LastName = model.LastName;
            //user.DateModified = DateTime.Now;

            //_userRepository.Save(user);

            //vs

            user.ChangeName(model.FirstName, model.LastName);

            _userRepository.Save(user);

            _domainEvents.Raise(new UserProfileWasEditedDomainEvent(user));

            return Ok();
        }
    }
}
