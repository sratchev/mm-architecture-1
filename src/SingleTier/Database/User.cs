using System;

namespace SingleTier.Web.Database
{
    public class User
    {
        public int Id { get; private set; }
        public string FirstName { get; private set; }
        public string LastName { get; private set; }
        public DateTime DateCreated { get; private set; }
        public DateTime DateModified { get; private set; }
        public string Password { get; private set; }
        public string PasswordSalt { get; private set; }
        public string FullName { get { return string.Format("{0} {1}", FirstName, LastName); } }

        public void ChangeName(string firstName, string lastName)
        {
            if (string.IsNullOrWhiteSpace(firstName)) throw new ArgumentNullException("firstName");
            if (string.IsNullOrWhiteSpace(lastName)) throw new ArgumentNullException("lastName");

            this.FirstName = firstName;
            this.LastName = lastName;
            this.DateModified = DateTime.Now;
        }
    }
}