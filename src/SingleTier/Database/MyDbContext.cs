﻿using System.Data.Entity;

namespace SingleTier.Web.Database
{
    public class MyDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
    }

}
