﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SingleTier.Web.Database
{
    public class UserRepository : IUserRepository
    {
        private readonly MyDbContext _context;

        public UserRepository(MyDbContext context)
        {
            if (context == null) throw new ArgumentNullException("context");

            _context = context;
        }

        public User GetById(int id)
        {
            return _context.Users.Find(id);
        }

        public void Save(User user)
        {
            throw new NotImplementedException();
        }
    }


}
