﻿namespace SingleTier.Web.Database
{
    public interface IUserRepository
    {
        User GetById(int id);
        void Save(User user);
    }
}