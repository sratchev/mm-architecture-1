﻿using System;
using System.Web.Mvc;
using SingleTier.Web.DomainEvents;
using SingleTier.Web.DomainEvents.Events;

namespace SingleTier.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IDomainEvents _domainEvents;

        public HomeController(IDomainEvents domainEvents)
        {
            if (domainEvents == null) throw new ArgumentNullException("domainEvents");

            _domainEvents = domainEvents;
        }

        public ActionResult Index()
        {
            ViewBag.Title = "Home Page";

            //todo: slide 2
            //throw new Exception("ELMAH error from MVC controller");

            _domainEvents.Raise(new HomeIndexWasAccessedDomainEvent(DateTime.Now));

            return View();
        }
    }
}
