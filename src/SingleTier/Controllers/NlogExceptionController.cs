using System;
using System.Web.Http;
using NLog;

namespace SingleTier.Web.Controllers
{
    public class NlogExceptionController : ApiController
    {
        private readonly Logger _logger;

        public NlogExceptionController()
        {
            _logger = LogManager.GetCurrentClassLogger();
        }

        public IHttpActionResult Get()
        {
            try
            {
                throw new Exception("Exception for nlog to catch");
            }
            catch (Exception ex)
            {
                _logger.Error(ex);
            }

            return Ok("Nlog error was thrown");
        }
    }
}