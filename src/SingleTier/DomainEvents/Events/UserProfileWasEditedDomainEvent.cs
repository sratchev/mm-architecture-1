﻿using SingleTier.Web.Database;

namespace SingleTier.Web.DomainEvents.Events
{
    public class UserProfileWasEditedDomainEvent : IDomainEvent
    {
        public User User { get; set; }

        public UserProfileWasEditedDomainEvent(User user)
        {
            User = user;
        }
    }
}
