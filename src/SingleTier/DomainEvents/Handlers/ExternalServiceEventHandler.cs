﻿using SingleTier.Web.DomainEvents.Events;

namespace SingleTier.Web.DomainEvents.Handlers
{
    public class ExternalServiceEventHandler : IHandleDomainEvent<UserProfileWasEditedDomainEvent>
    {
        public void Handle(UserProfileWasEditedDomainEvent args)
        {
            var user = args.User;

            //call an external api
        }
    }
}