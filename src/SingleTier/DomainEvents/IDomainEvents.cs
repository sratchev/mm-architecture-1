﻿namespace SingleTier.Web.DomainEvents
{
    public interface IDomainEvents
    {
        void Raise<T>(T domainEvent) where T : IDomainEvent;
    }
}