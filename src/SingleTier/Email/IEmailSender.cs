﻿using System.Net.Mail;

namespace SingleTier.Web.Email
{
    public interface IEmailSender
    {
        void Send(MailMessage mailMessage);
    }
}