﻿using System.Net.Mail;
using SingleTier.Web.Database;

namespace SingleTier.Web.Email
{
    public interface IEmailGenerator
    {
        MailMessage CreateWelcomeEmail(User user);
    }
}
