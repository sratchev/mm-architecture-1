﻿using Microsoft.Owin;
using Owin;
using SingleTier.Web;

[assembly: OwinStartup(typeof(Startup))]

namespace SingleTier.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
