﻿using System.Data.Entity;

namespace ThreeTier.Data.Database
{
    public class MyDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
    }

}
