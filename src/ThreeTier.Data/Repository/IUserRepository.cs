﻿using ThreeTier.Data.Database;

namespace ThreeTier.Data.Repository
{
    public interface IUserRepository
    {
        User GetById(int id);
        void Save(User user);
    }
}