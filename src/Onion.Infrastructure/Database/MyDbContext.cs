﻿using System.Data.Entity;
using Onion.Core.Entities;

namespace Onion.Infrastructure.Database
{
    public class MyDbContext : DbContext
    {
        public DbSet<User> Users { get; set; }
    }

}
