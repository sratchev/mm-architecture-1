﻿using System;
using Onion.Core.Events;
using SimpleInjector;

namespace Onion.Infrastructure.Events
{
    public class DomainEvents : IDomainEvents
    {
        private readonly Container _container;

        public DomainEvents(Container container)
        {
            if (container == null) throw new ArgumentNullException("container");

            _container = container;
        }


        public void Raise<T>(T eventArg) where T : IDomainEvent
        {
            foreach (var handler in _container.GetAllInstances<IHandleDomainEvent<T>>())
            {
                handler.Handle(eventArg);
            }
        }
    }
}
