﻿using Onion.Core.Events;

namespace Onion.Infrastructure.Events.Handlers
{
    public class ExternalServiceEventHandler : IHandleDomainEvent<UserProfileWasEditedDomainEvent>
    {
        public void Handle(UserProfileWasEditedDomainEvent args)
        {
            var user = args.User;

            //call an external api
        }
    }
}