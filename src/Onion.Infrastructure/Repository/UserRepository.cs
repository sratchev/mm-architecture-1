﻿using System;
using Onion.Core.Entities;
using Onion.Core.Repository;
using Onion.Infrastructure.Database;

namespace Onion.Infrastructure.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly MyDbContext _context;

        public UserRepository(MyDbContext context)
        {
            if (context == null) throw new ArgumentNullException("context");

            _context = context;
        }

        public User GetById(int id)
        {
            return _context.Users.Find(id);
        }

        public void Save(User user)
        {
            throw new NotImplementedException();
        }
    }


}
