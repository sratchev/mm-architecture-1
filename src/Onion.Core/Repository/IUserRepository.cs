﻿using Onion.Core.Entities;

namespace Onion.Core.Repository
{
    public interface IUserRepository
    {
        User GetById(int id);
        void Save(User user);
    }
}