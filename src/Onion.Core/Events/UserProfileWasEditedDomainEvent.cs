﻿using Onion.Core.Entities;

namespace Onion.Core.Events
{
    public class UserProfileWasEditedDomainEvent : IDomainEvent
    {
        public User User { get; set; }

        public UserProfileWasEditedDomainEvent(User user)
        {
            User = user;
        }
    }
}
