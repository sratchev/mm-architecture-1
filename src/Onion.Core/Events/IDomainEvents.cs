﻿namespace Onion.Core.Events
{
    public interface IDomainEvents
    {
        void Raise<T>(T domainEvent) where T : IDomainEvent;
    }
}