﻿using System;

namespace Onion.Core.Events
{
    public class HomeIndexWasAccessedDomainEvent : IDomainEvent
    {
        public DateTime DateTime { get; set; }

        public HomeIndexWasAccessedDomainEvent(DateTime dateTime)
        {
            DateTime = dateTime;
        }
    }
}