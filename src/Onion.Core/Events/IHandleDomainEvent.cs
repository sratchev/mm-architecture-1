﻿namespace Onion.Core.Events
{
    public interface IHandleDomainEvent<T> where T : IDomainEvent
    {
        void Handle(T args);
    }
}