﻿using System.Net.Mail;

namespace Onion.Core.Email
{
    public interface IEmailSender
    {
        void Send(MailMessage mailMessage);
    }
}