﻿using System.Net.Mail;
using Onion.Core.Entities;

namespace Onion.Core.Email
{
    public interface IEmailGenerator
    {
        MailMessage CreateWelcomeEmail(User user);
    }
}
