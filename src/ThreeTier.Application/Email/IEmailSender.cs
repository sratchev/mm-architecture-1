﻿using System.Net.Mail;

namespace ThreeTier.Application.Email
{
    public interface IEmailSender
    {
        void Send(MailMessage mailMessage);
    }
}