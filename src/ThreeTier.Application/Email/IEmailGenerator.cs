﻿using System.Net.Mail;
using ThreeTier.Data.Database;

namespace ThreeTier.Application.Email
{
    public interface IEmailGenerator
    {
        MailMessage CreateWelcomeEmail(User user);
    }
}
