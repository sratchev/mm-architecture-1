﻿using System;

namespace ThreeTier.Application.DomainEvents.Events
{
    public class HomeIndexWasAccessedDomainEvent : IDomainEvent
    {
        public DateTime DateTime { get; set; }

        public HomeIndexWasAccessedDomainEvent(DateTime dateTime)
        {
            DateTime = dateTime;
        }
    }
}