﻿using ThreeTier.Data.Database;

namespace ThreeTier.Application.DomainEvents.Events
{
    public class UserProfileWasEditedDomainEvent : IDomainEvent
    {
        public User User { get; set; }

        public UserProfileWasEditedDomainEvent(User user)
        {
            User = user;
        }
    }
}
