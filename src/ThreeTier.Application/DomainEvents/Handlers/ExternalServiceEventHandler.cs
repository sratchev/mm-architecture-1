﻿using ThreeTier.Application.DomainEvents.Events;

namespace ThreeTier.Application.DomainEvents.Handlers
{
    public class ExternalServiceEventHandler : IHandleDomainEvent<UserProfileWasEditedDomainEvent>
    {
        public void Handle(UserProfileWasEditedDomainEvent args)
        {
            var user = args.User;

            //call an external api
        }
    }
}