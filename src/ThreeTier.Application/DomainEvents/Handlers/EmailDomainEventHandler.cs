﻿using ThreeTier.Application.DomainEvents.Events;
using ThreeTier.Application.Email;

namespace ThreeTier.Application.DomainEvents.Handlers
{
    public class EmailDomainEventHandler : IHandleDomainEvent<UserProfileWasEditedDomainEvent>, IHandleDomainEvent<HomeIndexWasAccessedDomainEvent>
    {
        private readonly IEmailGenerator _emailGenerator;
        private readonly IEmailSender _emailSender;

        public EmailDomainEventHandler()
        {
            _emailGenerator = new EmailGenerator();
            _emailSender = new EmailSender();
        }


        ////todo: slide 5
        //public EmailDomainEventHandler(IEmailGenerator emailGenerator, IEmailSender emailSender)
        //{
        //    if (emailGenerator == null) throw new ArgumentNullException("emailGenerator");
        //    if (emailSender == null) throw new ArgumentNullException("emailSender");

        //    _emailGenerator = emailGenerator;
        //    _emailSender = emailSender;
        //}

        ////todo: poor man's DI
        //public EmailDomainEventHandler() : this(new EmailGenerator(), new EmailSender())
        //{

        //}


        public void Handle(UserProfileWasEditedDomainEvent args)
        {
            var user = args.User;

            var email = _emailGenerator.CreateWelcomeEmail(user);

            _emailSender.Send(email);
            //send out an email
        }

        public void Handle(HomeIndexWasAccessedDomainEvent args)
        {
            var time = args.DateTime;

            //send an email
        }
    }
}
