﻿namespace ThreeTier.Application.DomainEvents
{
    public interface IHandleDomainEvent<T> where T : IDomainEvent
    {
        void Handle(T args);
    }
}