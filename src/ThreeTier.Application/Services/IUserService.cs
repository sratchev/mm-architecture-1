namespace ThreeTier.Application.Services
{
    public interface IUserService
    {
        bool ChangeName(int userId, string firstName, string lastName);
    }
}