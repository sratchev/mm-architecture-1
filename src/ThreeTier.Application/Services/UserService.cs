﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ThreeTier.Application.DomainEvents;
using ThreeTier.Application.DomainEvents.Events;
using ThreeTier.Data.Database;
using ThreeTier.Data.Repository;

namespace ThreeTier.Application.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IDomainEvents _domainEvents;

        public UserService(IUserRepository userRepository, IDomainEvents domainEvents)
        {
            if (userRepository == null) throw new ArgumentNullException("userRepository");
            if (domainEvents == null) throw new ArgumentNullException("domainEvents");

            _userRepository = userRepository;
            _domainEvents = domainEvents;
        }

        public bool ChangeName(int userId, string firstName, string lastName)
        {
            var user = _userRepository.GetById(userId);

            user.ChangeName(firstName, lastName);

            try
            {
                _userRepository.Save(user);
                _domainEvents.Raise(new UserProfileWasEditedDomainEvent(user));

                return true;
            }
            catch (Exception ex)
            {
                //log
                return false;
            }

        }
    }
}
